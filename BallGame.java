public class BallGame {
    public static void playBasketball(){
        Player p = new Player("Ann", 0);
        Projectiles basketball = new Basketball();
        Target hoop = new Hoop(150);

        boolean hasProjectile = p.giveProjectile(basketball);
        if(hasProjectile){
            p.toss(hoop);
        }
        else{
            System.out.println("Nope");
        }
    }

    public static void play2PlayerBasketball(){
        Player p = new Player("Ann", 0);
        Player p2 = new Player("Bob", 4);
        Target hoop = new Hoop(8);
        Projectiles basketball = new Basketball();
        boolean hasProjectile = p.giveProjectile(basketball);
        if(hasProjectile){
            p.toss(p2);
        }
        else{
            System.out.println("Nope");
        }
    }

    public static void playBocce(){
        Player p = new Player("Ann",0);
        BocceBall b = new BocceBall(4);
        BocceBall b2 = new BocceBall(0);
        p.giveProjectile((Projectiles)b2);
        p.toss((Target)b);
        BocceBall b3 = new BocceBall(0);
        p.giveProjectile((Projectiles)b3);
        p.toss((Target)b2);
        Basketball basketBall = new Basketball();
        p.giveProjectile(basketBall);
        p.toss((Target)b2);
    }
}
