public class BocceBall implements Projectiles, Target {
    private double position;

    public BocceBall(double position){
        this.position = position;
    }
    public void recieve(Projectiles prj) {
        if(prj instanceof BocceBall){
            System.out.println("hit the Bocce ball");
            this.position += 0.5;
        }
        else{
            System.out.println("That wasn't a bocce ball");
        }
    }

    public double getPosition() {
        return this.position;
    }

    public void beTossed(Player per, Target tar) {
        double personPosition = per.getPosition();
        double targetPositon = tar.getPosition();
        double distance = personPosition + targetPositon;
        if(distance <= 3){
            tar.recieve(this);
            this.position = targetPositon;
            System.out.println("hit the Bocce ball");
        }
        else{
            System.out.println("missed the target");
            this.position += 3-targetPositon;
        }
        
    }
    
}
