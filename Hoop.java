public class Hoop implements Target {
    private double position;

    public Hoop(double position) {
        this.position = position;
    }

    public double getPosition(){
        return this.position;
    }

    public void recieve(Projectiles prj){
        if(prj instanceof Basketball){
            System.out.println("Got the basket");
        }
        else{
            System.out.println("That wasn't a basketball!");
        }
    }
}
