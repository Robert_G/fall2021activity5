import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class BasketballTest {
    @Test
    public void getNameTest(){
        
        Player p = new Player("Bob", 12);
        assertEquals("Bob",p.getName());
    }

    @Test
    public void getPositionTest(){
        
        Player p = new Player("Bob", 12);
        assertEquals(12,p.getPosition());
    }

    @Test
    public void giveProjectilePassedTest(){
        
        Projectiles b = new Basketball();
        Player p = new Player("Bob", 12);
        assertEquals(true,p.giveProjectile(b));
    }

    @Test
    public void giveProjectileFailedTest(){
        
        Projectiles b = new Basketball();
        Player p = new Player("Bob", 12);
        p.giveProjectile(b);
        assertEquals(false,p.giveProjectile(b));
    }


}
