public class Player implements Target {
    private String name;
    private double position;
    private Projectiles heldProjectile;

    public Player(String name, double position){
        this.name = name;
        this.position = position;
        this.heldProjectile = null;
    }

    public String getName(){
        return this.name;
    }
    public double getPosition(){
        return this.position;
    }

    public boolean giveProjectile(Projectiles projectile){
        if(this.heldProjectile == null){
            this.heldProjectile = projectile;
            return true;
        }
        else{
            return false;
        }
    }

    public boolean toss(Target target){
        if(this.heldProjectile == null){
            return false;
        }
        else{
            this.heldProjectile.beTossed(this,target);
            return true;
        }
    }

    public void recieve(Projectiles p){
        if(p == null){
            System.out.println("This person could not catch the ball!");
        }
        else{
            this.heldProjectile = p;
            System.out.println("Great catch!");
        }
    }
}
